﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Truckler
{

    public class Client
    {

        public int port { get; private set; }
        public string ipAddressStr { get; private set; }
        public IPAddress ipAddress { get; private set; }
        public IPEndPoint ipEndPoint { get; private set; }
        private Socket client;


        public delegate void Notify(ClientEventDetails e);
        public delegate void ServerAcceptEventHandler(Messages.ServerAccept serverAcceptMessage);
        public delegate void ServerDenyEventHandler(Messages.ServerDeny serverDenyMessage);
        public delegate void ServerShutdownEventHandler(Messages.ServerShutdown serverShutdownMessage);

        public delegate void SDKConfigurationHShifterEventHandler(Messages.SDKConfigurationHShifter message);
        public delegate void SDKConfigurationJobEventHandler(Messages.SDKConfigurationJob message);
        public delegate void SDKConfigurationShifterTypeEventHandler(Messages.SDKConfigurationShifterType message);
        public delegate void SDKConfigurationSubstancesEventHandler(Messages.SDKConfigurationSubstances message);
        public delegate void SDKConfigurationTrailerEventHandler(Messages.SDKConfigurationTrailer message);
        public delegate void SDKConfigurationTruckEventHandler(Messages.SDKConfigurationTruck message);
        public delegate void SDKGameplayEventJobCancelledEventHandler(Messages.SDKGameplayEventJobCancelled message);
        public delegate void SDKGameplayEventJobDeliveredEventHandler(Messages.SDKGameplayEventJobDelivered message);
        public delegate void SDKGameplayEventPlayerFinedEventHandler(Messages.SDKGameplayEventPlayerFined message);
        public delegate void SDKGameplayEventPlayerTollgatePaidEventHandler(Messages.SDKGameplayEventPlayerTollgatePaid message);
        public delegate void SDKGameplayEventPlayerUseFerryEventHandler(Messages.SDKGameplayEventPlayerUseFerry message);
        public delegate void SDKGameplayEventPlayerUseTrainEventHandler(Messages.SDKGameplayEventPlayerUseTrain message);
        public delegate void TrucklerGameplayEventEventHandler(Messages.TrucklerGameplayEvent message);
        public delegate void TrucklerPlayerInfoEventHandler(Messages.TrucklerPlayerInfo message);
        public delegate void TrucklerTelemetryFrameDataEventHandler(Messages.TrucklerTelemetryFrameData message);
        public delegate void SDKGamePauseEventHandler(Messages.SDKGamePause message);
        public delegate void SDKGameUnpauseEventHandler(Messages.SDKGameUnpause message);
        public delegate void UnknownEventHandler(Messages.Unknown message);


        public event Notify Error;
        public event Notify Connected;
        public event Notify Disconnected;
        public event ServerAcceptEventHandler ServerAccepted;
        public event ServerDenyEventHandler ServerDenied;
        public event ServerShutdownEventHandler ServerShutdown;
        public event SDKGamePauseEventHandler SDKGamePaused;
        public event SDKGameUnpauseEventHandler SDKGameUnpaused;
        public event SDKConfigurationHShifterEventHandler SDKConfigurationHShifter;
        public event SDKConfigurationJobEventHandler SDKConfigurationJob;
        public event SDKConfigurationShifterTypeEventHandler SDKConfigurationShifterType;
        public event SDKConfigurationSubstancesEventHandler SDKConfigurationSubstances;
        public event SDKConfigurationTrailerEventHandler SDKConfigurationTrailer;
        public event SDKConfigurationTruckEventHandler SDKConfigurationTruck;
        public event SDKGameplayEventJobCancelledEventHandler SDKGameplayEventJobCancelled;
        public event SDKGameplayEventJobDeliveredEventHandler SDKGameplayEventJobDelivered;
        public event SDKGameplayEventPlayerFinedEventHandler SDKGameplayEventPlayerFined;
        public event SDKGameplayEventPlayerTollgatePaidEventHandler SDKGameplayEventPlayerTollgatePaid;
        public event SDKGameplayEventPlayerUseFerryEventHandler SDKGameplayEventPlayerUseFerry;
        public event SDKGameplayEventPlayerUseTrainEventHandler SDKGameplayEventPlayerUseTrain;
        public event TrucklerGameplayEventEventHandler TrucklerGameplayEvent;
        public event TrucklerPlayerInfoEventHandler TrucklerPlayerInfo;
        public event TrucklerTelemetryFrameDataEventHandler TrucklerTelemetryFrameData;
        public event UnknownEventHandler UnknownMessageReceived;

        protected virtual void OnError(bool hasError, Exception e)
        {
            Error?.Invoke(new ClientEventDetails(hasError, e));
        }
        protected virtual void OnConnected(bool hasError, Exception? e)
        {
            Connected?.Invoke(new ClientEventDetails(hasError, e));
        }
        protected virtual void OnDisconnect(bool hasError, Exception? e)
        {
            Disconnected?.Invoke(new ClientEventDetails(hasError, e));
        }
        protected virtual void OnServerAccepted(Messages.ServerAccept message)
        {
            ServerAccepted?.Invoke(message);
        }
        protected virtual void OnServerDenied(Messages.ServerDeny message)
        {
            ServerDenied?.Invoke(message);
        }
        protected virtual void OnServerShutdown(Messages.ServerShutdown message)
        {
            ServerShutdown?.Invoke(message);
        }
        protected virtual void OnSDKGamePaused(Messages.SDKGamePause message)
        {
            SDKGamePaused?.Invoke(message);
        }
        protected virtual void OnSDKGameUnpaused(Messages.SDKGameUnpause message)
        {
            SDKGameUnpaused?.Invoke(message);
        }
        protected virtual void OnSDKConfigurationHShifter(Messages.SDKConfigurationHShifter message)
        {
            SDKConfigurationHShifter?.Invoke(message);
        }
        protected virtual void OnSDKConfigurationJob(Messages.SDKConfigurationJob message)
        {
            SDKConfigurationJob?.Invoke(message);
        }
        protected virtual void OnSDKConfigurationShifterType(Messages.SDKConfigurationShifterType message)
        {
            SDKConfigurationShifterType?.Invoke(message);
        }
        protected virtual void OnSDKConfigurationSubstances(Messages.SDKConfigurationSubstances message)
        {
            SDKConfigurationSubstances?.Invoke(message);
        }
        protected virtual void OnSDKConfigurationTrailer(Messages.SDKConfigurationTrailer message)
        {
            SDKConfigurationTrailer?.Invoke(message);
        }
        protected virtual void OnSDKConfigurationTruck(Messages.SDKConfigurationTruck message)
        {
            SDKConfigurationTruck?.Invoke(message);
        }
        protected virtual void OnSDKGameplayEventJobCancelled(Messages.SDKGameplayEventJobCancelled message)
        {
            SDKGameplayEventJobCancelled?.Invoke(message);
        }
        protected virtual void OnSDKGameplayEventJobDelivered(Messages.SDKGameplayEventJobDelivered message)
        {
            SDKGameplayEventJobDelivered?.Invoke(message);
        }
        protected virtual void OnSDKGameplayEventPlayerFined(Messages.SDKGameplayEventPlayerFined message)
        {
            SDKGameplayEventPlayerFined?.Invoke(message);
        }
        protected virtual void OnSDKGameplayEventPlayerTollgatePaid(Messages.SDKGameplayEventPlayerTollgatePaid message)
        {
            SDKGameplayEventPlayerTollgatePaid?.Invoke(message);
        }
        protected virtual void OnSDKGameplayEventPlayerUseFerry(Messages.SDKGameplayEventPlayerUseFerry message)
        {
            SDKGameplayEventPlayerUseFerry?.Invoke(message);
        }
        protected virtual void OnSDKGameplayEventPlayerUseTrain(Messages.SDKGameplayEventPlayerUseTrain message)
        {
            SDKGameplayEventPlayerUseTrain?.Invoke(message);
        }
        protected virtual void OnTrucklerGameplayEvent(Messages.TrucklerGameplayEvent message)
        {
            TrucklerGameplayEvent?.Invoke(message);
        }
        protected virtual void OnTrucklerPlayerInfo(Messages.TrucklerPlayerInfo message)
        {
            TrucklerPlayerInfo?.Invoke(message);
        }
        protected virtual void OnTrucklerTelemetryFrameData(Messages.TrucklerTelemetryFrameData message)
        {
            TrucklerTelemetryFrameData?.Invoke(message);
        }
        protected virtual void OnUnknownMessageReceived(Messages.Unknown message)
        {
            UnknownMessageReceived?.Invoke(message);
        }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public Client()
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        {
           
        }
        public void ConnectDefault()
        {
            Connect("127.0.0.1", 40000);
        }
        public void Connect(IPAddress _ipAddress, int _port)
        {
            this.ipAddress = _ipAddress;
            this.port = _port;
            this.ipAddressStr = _ipAddress.ToString();
            this.ipEndPoint = new IPEndPoint(this.ipAddress, this.port);
            this.Connect();

        }
        public void Connect(IPEndPoint _ipEndPoint)
        {
            this.ipAddress = _ipEndPoint.Address;
            this.port = _ipEndPoint.Port;
            this.ipAddressStr = _ipEndPoint.Address.ToString();
            this.ipEndPoint = _ipEndPoint;
            this.Connect();
        }


        public void Connect(string _ipAddress, int _port)
        {
            this.port = _port;
            this.ipAddressStr = _ipAddress;
            bool success = false;
            try
            {
                this.ipAddress = IPAddress.Parse(this.ipAddressStr);
                this.ipEndPoint = new IPEndPoint(this.ipAddress, this.port);
                success = true;
            }
            catch (Exception ex)
            {
                OnError(true, ex);
            }
            if (success)
            {
                this.Connect();
            }

        }

        private void Connect()
        {

                client = new Socket(ipAddress.AddressFamily,
                        SocketType.Stream, ProtocolType.Tcp);
           

            // Connect to the remote endpoint.  
            client.BeginConnect(ipEndPoint,
                new AsyncCallback(ConnectCallback), client);

        }
        public void Disconnect()
        {
            try { client?.Shutdown(SocketShutdown.Both); } catch { }
            try { client?.Disconnect(false); } catch { }
            try { client?.Close(); } catch { }
            try { client?.Dispose(); } catch { }
            OnDisconnect(false, null);
        }

        public bool IsConnected()
        {
            return client == null ? false : client.Connected;
        }
        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                if (ar.AsyncState != null)
                {
                    Socket client = (Socket)ar.AsyncState;

                    // Complete the connection.  
                    client.EndConnect(ar);

                    OnConnected(false, null);

                    StateObject state = new StateObject();
                    state.workSocket = client;
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), state);
                }

            }
            catch (Exception e)
            {
                OnError(true, e);
            }
        }

 

        private void ReceiveCallback(IAsyncResult ar)
        {
            if (ar.AsyncState != null)
            {
                StateObject state = (StateObject)ar.AsyncState;
                if (state.workSocket != null)
                {
                    Socket handler = state.workSocket;

                    if (handler != null && handler.Connected)
                    {
                        // Read data from the client socket.   
                        int bytesRead = handler.EndReceive(ar);
                        state.szCount += bytesRead;
                        if (bytesRead > 0 || state.szCount > 0)
                        {

                            if (bytesRead > 0)
                            {
                                state.bb.AddRange(state.buffer);
                            }

                            if (state.szCount >= sizeof(int) * 2 && !state.szReceived)
                            {
                                state.szReceived = true;
                                state.sz = BitConverter.ToInt32(state.bb.ToArray(), sizeof(int));
                            }

                            if (state.szCount < state.sz + sizeof(int) + sizeof(int))
                            {
                                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                                new AsyncCallback(ReceiveCallback), state);
                            }
                            else
                            {
                                byte[] trueData = state.bb.Take(state.sz + sizeof(int) + sizeof(int)).ToArray();
                                state.bb = state.bb.Skip(state.sz + sizeof(int) + sizeof(int)).ToList();


                                ProcessNewMessage(trueData);
                                //OnNewData(new DataClass() { Data = trueData.ToList() });
                                List<byte> overflow = new List<byte>();
                                if (state.szCount - sizeof(int) - sizeof(int) > state.sz)
                                {
                                    overflow.AddRange(state.bb.Take(state.szCount - state.sz - sizeof(int) - sizeof(int)).ToArray());
                                }
                                int remainingToProcess = overflow.Count;
                                while (remainingToProcess > 0)
                                {
                                    state.bb.Clear();
                                    state.bb.AddRange(overflow.ToArray());
                                    state.szCount = state.bb.Count;
                                    state.sz = 0;
                                    state.szReceived = state.szCount >= sizeof(int) * 2;
                                    if (state.szReceived)
                                    {
                                        state.sz = state.sz = BitConverter.ToInt32(state.bb.ToArray(), sizeof(int));
                                    }
                                    if (state.bb.Count >= state.sz && state.sz > 0 && state.szReceived)
                                    {
                                        ProcessNewMessage(state.bb.Take(state.sz + sizeof(int) + sizeof(int)).ToArray());
                                        //OnNewData(new DataClass() { Data = state.bb.Take(state.sz+sizeof(int) + sizeof(int)).ToList() });
                                        overflow = overflow.Skip(state.sz + sizeof(int) + sizeof(int)).ToList();
                                        remainingToProcess = overflow.Count;
                                    }
                                    else
                                    {
                                        remainingToProcess = 0;
                                    }
                                }
                                if (state.szCount - sizeof(int) - sizeof(int) == state.sz)
                                {
                                    state.sz = 0;
                                    state.bb.Clear();
                                    state.szCount = 0;
                                    state.szReceived = false;
                                }

                                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                                new AsyncCallback(ReceiveCallback), state);
                            }

                        }
                    }
                }
            }

        }
        public void SendMessage(Messages.BaseMessage message)
        {
            SendBytes(message.Raw);
        }
        private void SendBytes(byte[] data)
        {
            try
            {
                client.BeginSend(data.ToArray(), 0, data.Length, 0,
                    new AsyncCallback(SendCallback), client);
            }
            catch (SocketException e)
            {
                OnError(true, e);
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                if (ar.AsyncState != null)
                {
                    Socket client = (Socket)ar.AsyncState;

                    int bytesSent = client.EndSend(ar);
                }
            }
            catch (Exception e)
            {
                OnError(true, e);
            }
        }

        private void ProcessNewMessage(byte[] data)
        {
            int msgType = BitConverter.ToInt32(data, 0);
            Messages.EMessageType eMsgType = Messages.MessageType.IntToMessageType(msgType);
            switch(eMsgType)
            {
                case Messages.EMessageType.ServerAccept:
                    OnServerAccepted(new Messages.ServerAccept(data));
                    break;
                case Messages.EMessageType.ServerDeny:
                    OnServerDenied(new Messages.ServerDeny(data));
                    break;
                case Messages.EMessageType.ServerShutdown:
                    OnServerShutdown(new Messages.ServerShutdown(data));
                    break;
                case Messages.EMessageType.SDK_Configuration_HShifter:
                    OnSDKConfigurationHShifter(new Messages.SDKConfigurationHShifter(data));
                    break;
                case Messages.EMessageType.SDK_Configuration_Job:
                    OnSDKConfigurationJob(new Messages.SDKConfigurationJob(data));
                    break;
                case Messages.EMessageType.SDK_Configuration_Substances:
                    OnSDKConfigurationSubstances(new Messages.SDKConfigurationSubstances(data));
                    break;
                case Messages.EMessageType.SDK_Configuration_Trailer:
                    OnSDKConfigurationTrailer(new Messages.SDKConfigurationTrailer(data));
                    break;
                case Messages.EMessageType.SDK_Conifguration_ShifterType:
                    OnSDKConfigurationShifterType(new Messages.SDKConfigurationShifterType(data));
                    break;
                case Messages.EMessageType.SDK_Configuration_Truck:
                    OnSDKConfigurationTruck(new Messages.SDKConfigurationTruck(data));
                    break;
                case Messages.EMessageType.SDK_Game_Pause:
                    OnSDKGamePaused(new Messages.SDKGamePause(data));
                    break;
                case Messages.EMessageType.SDK_Game_Unpause:
                    OnSDKGameUnpaused(new Messages.SDKGameUnpause(data));
                    break;
                case Messages.EMessageType.Truckler_Gameplay_Event:
                    OnTrucklerGameplayEvent(new Messages.TrucklerGameplayEvent(data));
                    break;
                case Messages.EMessageType.Truckler_Player_Info:
                    OnTrucklerPlayerInfo(new Messages.TrucklerPlayerInfo(data));
                    break;
                case Messages.EMessageType.Truckler_Telemetry_Frame_Data:
                    OnTrucklerTelemetryFrameData(new Messages.TrucklerTelemetryFrameData(data));
                    break;
                case Messages.EMessageType.SDK_Gameplay_Event_Job_Cancelled:
                    OnSDKGameplayEventJobCancelled(new Messages.SDKGameplayEventJobCancelled(data));
                    break;
                case Messages.EMessageType.SDK_Gameplay_Event_Job_Delivered:
                    OnSDKGameplayEventJobDelivered(new Messages.SDKGameplayEventJobDelivered(data));
                    break;
                case Messages.EMessageType.SDK_Gameplay_Event_Player_Fined:
                    OnSDKGameplayEventPlayerFined(new Messages.SDKGameplayEventPlayerFined(data));
                    break;
                case Messages.EMessageType.SDK_Gameplay_Event_Player_Tollgate_Paid:
                    OnSDKGameplayEventPlayerTollgatePaid(new Messages.SDKGameplayEventPlayerTollgatePaid(data));
                    break;
                case Messages.EMessageType.SDK_Gameplay_Event_Player_Use_Ferry:
                    OnSDKGameplayEventPlayerUseFerry(new Messages.SDKGameplayEventPlayerUseFerry(data));
                    break;
                case Messages.EMessageType.SDK_Gameplay_Event_Player_Use_Train:
                    OnSDKGameplayEventPlayerUseTrain(new Messages.SDKGameplayEventPlayerUseTrain(data));
                    break;

                default:
                    OnUnknownMessageReceived(new Messages.Unknown(data));
                    break;
            }
        }
    }
}
