﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class TrucklerGameplayEvent : BaseMessage
    {
        [JsonProperty("eventId")]
        public readonly string EventId;

        [JsonProperty("offense")]
        public readonly string Offense;

        [JsonProperty("source")]
        public readonly string Source;
        [JsonProperty("sourceId")]
        public readonly string SourceId;
        [JsonProperty("destination")]
        public readonly string Destination;
        [JsonProperty("destinationId")]
        public readonly string DestinationId;
        [JsonProperty("amount")]
        public readonly long Amount;
        [JsonProperty("location")]
        public readonly Objects.Truckler.Placement Location; 
        [JsonProperty("truckDamages")]
        public readonly Objects.Truckler.Damages TruckDamages;
        [JsonProperty("trailerDamages")]
        public readonly Objects.Truckler.Damages[] TrailerDamages; 
        [JsonProperty("timestamp")]
        public readonly long Timestamp;
        [JsonProperty("gameTime")]
        public readonly uint GameTime;
        [JsonProperty("speed")]
        public readonly float Speed;
        [JsonProperty("speedLimit")]
        public readonly float SpeedLimit;
        [JsonProperty("gameGenerated")]
        public readonly bool GameGenerated;
        internal TrucklerGameplayEvent(IEnumerable<byte> data) : base(data)
        {
            //initialize properties
            EventId = String.Empty;
            Offense = String.Empty;
            Source = String.Empty;
            Destination = String.Empty;
            SourceId = String.Empty;
            DestinationId = String.Empty;
            Amount = 0;
            Location = new Objects.Truckler.Placement();
            TruckDamages = new Objects.Truckler.Damages();
            TrailerDamages = new Objects.Truckler.Damages[0];
            Timestamp = 0;
            GameTime = 0;
            Speed = 0.00f;
            SpeedLimit = 0.00f;
            GameGenerated = false;
            //---------------------
            if (HasError)
            {
                return;
            }
            try
            {
                //process message
                new JsonSerializer().Populate(new JsonTextReader(new StringReader(System.Text.UTF8Encoding.UTF8.GetString(Raw))), this);
                //---------------
                HasError = false;
                Exception = null;
            }
            catch (Exception ex)
            {
                HasError = true;
                Exception = ex;
            }
        }

    }
}
