﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class Unknown : BaseMessage
    {
        public Unknown(IEnumerable<byte> data) : base(data)
        {

        }
    }
}
