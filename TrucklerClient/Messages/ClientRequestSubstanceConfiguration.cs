﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientRequestSubstanceConfiguration : BaseMessage
    {
        internal ClientRequestSubstanceConfiguration(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientRequestSubstanceConfiguration Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Request_Substance_Configuration));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientRequestSubstanceConfiguration(data);
        }
    }
}
