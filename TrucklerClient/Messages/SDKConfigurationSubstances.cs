﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class SDKConfigurationSubstances : BaseMessage
    {
        [JsonProperty("substances")]

        public readonly string[] Substances;
        internal SDKConfigurationSubstances(IEnumerable<byte> data) : base(data)
        {
            //initialize properties
            Substances = new string[] { };
            //---------------------
            if (HasError)
            {
                return;
            }
            try
            {
                //process message
                new JsonSerializer().Populate(new JsonTextReader(new StringReader(System.Text.UTF8Encoding.UTF8.GetString(Raw))), this);
                //---------------
                HasError = false;
                Exception = null;
            }
            catch (Exception ex)
            {
                HasError = true;
                Exception = ex;
            }

        }
    }
}
