﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientRequestTruckConfiguration : BaseMessage
    {
        internal ClientRequestTruckConfiguration(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientRequestTruckConfiguration Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Request_Truck_Configuration));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientRequestTruckConfiguration(data);
        }
    }
}
