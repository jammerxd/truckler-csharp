﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ServerShutdown : BaseMessage
    {
        internal ServerShutdown(IEnumerable<byte> data) : base(data)
        {
            if (HasError)
            {
                return;
            }
            Exception = null;
        }
    }
}
