﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class SDKConfigurationTruck : BaseMessage
    {
        [JsonProperty("truck")]
        public readonly Objects.TruckConfiguration Truck;
        internal SDKConfigurationTruck(IEnumerable<byte> data) : base(data)
        {
            //initialize properties
            Truck = new Objects.TruckConfiguration();
            //---------------------
            if (HasError)
            {
                return;
            }
            try
            {
                //process message
                new JsonSerializer().Populate(new JsonTextReader(new StringReader(System.Text.UTF8Encoding.UTF8.GetString(Raw))), this);
                //---------------
                HasError = false;
                Exception = null;
            }
            catch (Exception ex)
            {
                HasError = true;
                Exception = ex;
            }
        }
    }
}
