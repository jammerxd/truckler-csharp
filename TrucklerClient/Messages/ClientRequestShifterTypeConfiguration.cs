﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientRequestShifterTypeConfiguration : BaseMessage
    {
        internal ClientRequestShifterTypeConfiguration(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientRequestShifterTypeConfiguration Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Request_ShifterType_Configuration));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientRequestShifterTypeConfiguration(data);
        }
    }
}
