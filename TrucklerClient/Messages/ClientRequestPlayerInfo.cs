﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Messages
{
    public class ClientRequestPlayerInfo : BaseMessage
    {
        internal ClientRequestPlayerInfo(IEnumerable<byte> data) : base(data,false)
        {
            HasError = false;
            Exception = null;
        }
        public static ClientRequestPlayerInfo Build()
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(Messages.MessageType.Client_Request_PlayerInfo));
            data.AddRange(BitConverter.GetBytes(0));
            return new ClientRequestPlayerInfo(data);
        }
    }
}
