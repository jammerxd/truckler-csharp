﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Truckler
{
    public class StateObject
    {
        // Client  socket.  
        public Socket? workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 1024;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        //received buffer
        public List<byte> bb = new List<byte>();
        //size of data to expect (bytes)
        public int sz;
        //have we received the first bytes?
        public bool szReceived = false;

        //how many bytes have we actually received?
        public int szCount;



    }
}
