﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects
{
    public class TruckConfiguration
    {
        [JsonProperty("wheels")]
        public Objects.WheelConfiguration[] Wheels {get; protected internal set; }
        
        [JsonProperty("reverseRatios")]
        public float[] ReverseRatios { get; protected internal set; }
        
        [JsonProperty("forwardRatios")]
        public float[] ForwardRatios { get; protected internal set; }

        [JsonProperty("makeId")]
        public string MakeId { get; protected internal set; }
        
        [JsonProperty("make")]
        public string Make { get; protected internal set; }

        [JsonProperty("model")]
        public string Model { get; protected internal set; }

        [JsonProperty("modelId")]
        public string ModelId { get; protected internal set; }
       
        [JsonProperty("licensePlate")]
        public string LicensePlate { get; internal protected set; }

        [JsonProperty("licensePlateCountry")]
        public string LicensePlateCountry { get; internal protected set; }

        [JsonProperty("licensePlateCountryId")]
        public string LicensePlateCountryId { get; internal protected set; }

        [JsonProperty("fuelCapacity")]
        public float FuelCapacity { get; internal protected set; }

        [JsonProperty("fuelWarningFactor")]
        public float FuelWarningFactor { get; internal protected set; }

        [JsonProperty("adBlueCapacity")]
        public float AdBlueCapacity { get; internal protected set; }

        [JsonProperty("adBlueWarningFactor")]
        public float AdBlueWarningFactor { get; internal protected set; }

        [JsonProperty("airPressureEmergencyFactor")]
        public float AirPressureEmergencyFactor { get; internal protected set; }

        [JsonProperty("airPressureWarningFactor")]
        public float AirPressureWarningFactor { get; internal protected set; }

        [JsonProperty("batteryVoltageWarningFactor")]
        public float BatteryVoltageWarningFactor { get; internal protected set; }

        [JsonProperty("forwardGearCount")]
        public uint ForwardGearCount { get; internal protected set; }

        [JsonProperty("oilPressureWarningFactor")]
        public float OilPressureWarningFactor { get; internal protected set; }

        [JsonProperty("retarderStepCount")]
        public uint RetarderStepCount { get; internal protected set; }

        [JsonProperty("reverseGearCount")]
        public uint ReverseGearCount { get; internal protected set; }
        
        [JsonProperty("rpmLimit")]
        public float RPMLimit { get; internal protected set; }

        [JsonProperty("waterTemperatureWarningFactor")]
        public float WaterTemperatureWarningFactor { get; internal protected set; }

        [JsonProperty("wheelCount")]
        public uint WheelCount { get; internal protected set; }
        
        [JsonProperty("hasTruck")]
        public bool HasTruck { get; internal protected set; }
        internal protected TruckConfiguration()
        {
            Wheels = new WheelConfiguration[0];
            ReverseRatios = new float[0];
            ForwardRatios = new float[0];
            Make = String.Empty;
            MakeId = String.Empty;
            Model = String.Empty;
            ModelId = String.Empty;
            LicensePlate = String.Empty;
            LicensePlateCountry = String.Empty;
            LicensePlateCountryId = String.Empty;
            FuelCapacity = 0.00f;
            FuelWarningFactor = 0.00f;
            AdBlueCapacity = 0.00f;
            AdBlueWarningFactor = 0.00f;
            AirPressureEmergencyFactor = 0.00f;
            AirPressureWarningFactor = 0.00f;
            BatteryVoltageWarningFactor = 0.00f;
            ForwardGearCount = 0;
            OilPressureWarningFactor = 0.00f;
            RetarderStepCount = 0;
            ReverseGearCount = 0;
            RPMLimit = 0.00f;
            WaterTemperatureWarningFactor = 0.00f;
            WheelCount = 0;
            HasTruck = false;

        }
    }
}
