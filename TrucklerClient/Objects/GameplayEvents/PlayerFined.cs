﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.GameplayEvents
{
    public class PlayerFined
    {
        [JsonProperty("offense")]
        public string Offense { get; protected internal set; }

        [JsonProperty("payAmount")]
        public long Amount { get; protected internal set; }

        internal protected PlayerFined()
        {
            Offense = String.Empty;
            Amount = 0;
        }
    }
}
