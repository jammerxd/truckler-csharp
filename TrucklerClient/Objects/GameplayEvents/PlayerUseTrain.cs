﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.GameplayEvents
{

    public class PlayerUseFerry
    {
        [JsonProperty("source")]
        public string Source { get; protected internal set; }

        [JsonProperty("sourceId")]
        public string SourceId { get; protected internal set; }

        [JsonProperty("destination")]
        public string Destination { get; protected internal set; }

        [JsonProperty("destinationId")]
        public string DestinationId { get; protected internal set; }

        [JsonProperty("payAmount")]
        public long Amount { get; protected internal set; }

        protected internal PlayerUseFerry()
        {
            Amount = 0;
            Source = String.Empty;
            SourceId = String.Empty;
            Destination = String.Empty;
            DestinationId = String.Empty;
        }
    }
}
