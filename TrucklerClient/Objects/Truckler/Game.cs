﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    
    public class Game
    {
        [JsonProperty("gameId")]
        public string Id { get; internal protected set; }
        [JsonProperty("gameName")]
        public string Name { get; internal protected set; }
        [JsonProperty("gameVersionStr")]
        public string VersionStr { get; internal protected set; }
        [JsonProperty("gameVersionOnlyStr")]
        public string VersionOnlyStr { get; internal protected set; }
        [JsonProperty("osEnvironment")]
        public string OSEnvironment { get; internal protected set; }
        [JsonProperty("osArchitecture")]
        public string OSArchitecture { get; internal protected set; }
        [JsonProperty("majorVersion")]
        public int MajorVersion { get; internal protected set; }
        [JsonProperty("minorVersion")]
        public int MinorVersion { get; internal protected set; }
        [JsonProperty("isTruckersMP")]
        public bool IsTruckersMP { get; internal protected set; }
        [JsonProperty("truckersMPTempId")]
        public string TruckersMPTempId { get; internal protected set; }
        [JsonProperty("truckersMPServer")]
        public string TruckersMPServer { get; internal protected set; }

        internal protected Game()
        {
            Id = String.Empty;
            Name = String.Empty;
            VersionStr = String.Empty;
            VersionOnlyStr = String.Empty;
            OSEnvironment = String.Empty; 
            OSArchitecture = String.Empty;
            MajorVersion = 0;
            MinorVersion = 0; 
            IsTruckersMP = false;
            TruckersMPTempId = String.Empty;
            TruckersMPServer = String.Empty;
        }
    }
}
