﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    
    public class Plugin
    {
        [JsonProperty("majorVersion")]
        public int MajorVersion { get; internal protected set; }
        [JsonProperty("minorVersion")]
        public int MinorVersion { get; internal protected set; }
        [JsonProperty("buildVersion")]
        public int BuildVersion { get; internal protected set; }
        [JsonProperty("pluginVersionOnlyStr")]
        public string VersionOnlyStr { get; internal protected set; }
        internal protected Plugin()
        {
            MajorVersion = 0;
            MinorVersion = 0;
            BuildVersion = 0;
            VersionOnlyStr = String.Empty;

        }
    }
}
