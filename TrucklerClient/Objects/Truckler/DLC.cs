﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    public class DLC
    {
        [JsonProperty("name")]
        public string Name { get; internal protected set; }

        [JsonProperty("appid")]
        public string AppId { get; internal protected set; }

        [JsonProperty("available")]
        public bool Available { get; internal protected set; }

        [JsonProperty("installed")]
        public bool Installed { get; internal protected set; }
        internal protected DLC()
        {
            Name = String.Empty;
            AppId = String.Empty;
            Available = false;
            Installed = false;
        }
    }
}
