﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler
{
    public class JobDelivered
    {
        [JsonProperty("startX")]
        public double StartX { get; internal protected set; }

        [JsonProperty("startY")]
        public double StartY { get; internal protected set; }

        [JsonProperty("startZ")]
        public double StartZ { get; internal protected set; }

        [JsonProperty("startEpoch")]
        public long JobStartedEpoch { get; internal protected set; }

        [JsonProperty("startGameTime")]
        public uint GameMinuteStarted { get; internal protected set; }

        [JsonProperty("cargoDamage")]
        public float CargoDamage { get; internal protected set; }

        [JsonProperty("actualCargoDamage")]
        public float ActualCargoDamage { get; internal protected set; }

        [JsonProperty("isLate")]
        public bool IsLate { get; internal protected set; }

        [JsonProperty("timeRemaining")]
        public uint TimeRemaining { get; internal protected set; }

        [JsonProperty("fuelBurned")]
        public float FuelBurned { get; internal protected set; }

        [JsonProperty("fuelPurchased")]
        public float FuelPurchased { get; internal protected set; }

        [JsonProperty("topSpeed")]
        public float TopSpeed { get; internal protected set; }

        [JsonProperty("endX")]
        public double EndX { get; internal protected set; }

        [JsonProperty("endY")]
        public double EndY { get; internal protected set; }

        [JsonProperty("endZ")]
        public double EndZ { get; internal protected set; }
        internal protected JobDelivered()
        {
            StartX = 0.00f;
            StartY = 0.00f;
            StartZ = 0.00f;
            JobStartedEpoch = 0;
            GameMinuteStarted = 0;
            CargoDamage = 0.00f;
            ActualCargoDamage = 0.00f;
            IsLate = false;
            TimeRemaining = 0;
            FuelBurned = 0.00f;
            FuelPurchased = 0.00f;
            TopSpeed = 0.00f;
            EndX = 0.00f;
            EndY = 0.00f;
            EndZ = 0.00f;
        }
    }
}
