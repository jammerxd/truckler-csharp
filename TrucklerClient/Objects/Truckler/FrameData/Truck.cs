﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler.FrameData
{
    public class Truck
    {
        [JsonProperty("odometer")]
        public float Odometer { get; internal protected set; }
        [JsonProperty("speed")]
        public float Speed { get; internal protected set; }
        [JsonProperty("placement")]
        public Objects.Truckler.Placement Location { get; internal protected set; }
        [JsonProperty("engineEnabled")]
        public bool EngineEnabled { get; internal protected set; }
        
        [JsonProperty("gear")]
        public int Gear { get; internal protected set; }
        
        [JsonProperty("gearDisplayed")]
        public int GearDisplayed { get; internal protected set; }
        
        [JsonProperty("engineRPM")]
        public float EngineRPM { get; internal protected set; }
        
        [JsonProperty("retarderBrakeLevel")]
        public uint RetarderBrakeLevel { get; internal protected set; }
        
        [JsonProperty("brakeTemperature")]
        public float BrakeTemperature { get; internal protected set; }
        
        [JsonProperty("fuelRange")]
        public float FuelRange { get; internal protected set; }
        
        [JsonProperty("oilPressure")]
        public float OilPressure { get; internal protected set; }
        
        [JsonProperty("oilTemperature")]
        public float OilTemperature { get; internal protected set; }
        
        [JsonProperty("waterTemperature")]
        public float WaterTemperature { get; internal protected set; }
        
        [JsonProperty("batteryVoltage")]
        public float BatteryVoltage { get; internal protected set; }
        
        [JsonProperty("wipersOn")]
        public bool WipersOn { get; internal protected set; }
        
        [JsonProperty("parkingBrake")]
        public bool ParkingBrake { get; internal protected set; }
        
        [JsonProperty("motorBrake")]
        public bool MotorBrake { get; internal protected set; }
        
        [JsonProperty("electricsEnabled")]
        public bool ElectricsEnabled { get; internal protected set; }
        
        [JsonProperty("inputSteering")]
        public float InputSteering { get; internal protected set; }
        
        [JsonProperty("inputThrottle")]
        public float InputThrottle { get; internal protected set; }
        
        [JsonProperty("inputBrake")]
        public float InputBrake { get; internal protected set; }
        
        [JsonProperty("inputClutch")]
        public float InputClutch { get; internal protected set; }
        
        [JsonProperty("effectiveSteering")]
        public float EffectiveSteering { get; internal protected set; }
        
        [JsonProperty("effectiveThrottle")]
        public float EffectiveThrottle { get; internal protected set; }
        
        [JsonProperty("effectiveBrake")]
        public float EffectiveBrake { get; internal protected set; }
        
        [JsonProperty("effectiveClutch")]
        public float EffectiveClutch { get; internal protected set; }
        
        [JsonProperty("cruiseControl")]
        public float CruiseControl { get; internal protected set; }
        
        [JsonProperty("airPressure")]
        public float AirPressure { get; internal protected set; }
      
        [JsonProperty("airPressureWarning")]
        public bool AirPressureWarning { get; internal protected set; }
        
        [JsonProperty("AirPressureEmergency")]
        public bool AirPressureEmergency { get; internal protected set; }
        
        [JsonProperty("fuelCurrentLitres")]
        public float FuelCurrentLitres { get; internal protected set; }
        
        [JsonProperty("fuelWarning")]
        public bool FuelWarning { get; internal protected set; }
        
        [JsonProperty("fuelConsumptionAverage")]
        public float FuelConsumptionAverage { get; internal protected set; }
        
        [JsonProperty("adBlue")]
        public float AdBlue { get; internal protected set; }
        
        [JsonProperty("adBlueWarning")]
        public bool AdBlueWarning { get; internal protected set; }
        
        [JsonProperty("adBlueConsumptionAverage")]
        public float AdBlueConsumptionAverage { get; internal protected set; }
        
        [JsonProperty("oilPressureWarning")]
        public bool OilPressureWarning { get; internal protected set; }
        
        [JsonProperty("waterTemperatureWarning")]
        public bool WaterTemperatureWarning { get; internal protected set; }
        
        [JsonProperty("batteryVoltageWarning")]
        public bool BatteryVoltageWarning { get; internal protected set; }
        
        [JsonProperty("lightLowBeams")]
        public bool LightLowBeams { get; internal protected set; }
        
        [JsonProperty("lightHighBeams")]
        public bool LightHighBeams { get; internal protected set; }
        
        [JsonProperty("lightLeftBlinkerLogical")]
        public bool LightLeftBlinkerLogical { get; internal protected set; }
        
        [JsonProperty("lightRightBlinkerLogical")]
        public bool LightRightBlinkerLogical { get; internal protected set; }
        
        [JsonProperty("lightHazardWarningLogical")]
        public bool LightHazardWarningLogical { get; internal protected set; }
        
        [JsonProperty("lightLeftBlinker")]
        public bool LightLeftBlinker { get; internal protected set; }
        
        [JsonProperty("lightRightBlinker")]
        public bool LightRightBlinker { get; internal protected set; }
        
        [JsonProperty("lightParking")]
        public bool LightParking { get; internal protected set; }
        
        [JsonProperty("lightAuxiliaryFront")]
        public uint LightAuxiliaryFront { get; internal protected set; }
        
        [JsonProperty("lightAuxiliaryRoof")]
        public uint LightAuxiliaryRoof { get; internal protected set; }
        
        [JsonProperty("lightBeacon")]
        public bool LightBeacon { get; internal protected set; }
        
        [JsonProperty("lightBrake")]
        public bool LightBrake { get; internal protected set; }
        
        [JsonProperty("lightReverse")]
        public bool LightReverse { get; internal protected set; }
        
        [JsonProperty("lightDasboardBacklightFactor")]
        public float LightDasboardBacklightFactor { get; internal protected set; }
        
        [JsonProperty("differentialLockEnabled")]
        public bool DifferentialLockEnabled { get; internal protected set; }
        
        [JsonProperty("liftAxleLifted")]
        public bool LiftAxleLifted { get; internal protected set; }
        
        [JsonProperty("liftAxleIndicatorLit")]
        public bool LiftAxleIndicatorLit { get; internal protected set; }
        
        [JsonProperty("liftAxleTrailerLifted")]
        public bool LiftAxleTrailerLifted { get; internal protected set; }
        
        [JsonProperty("liftAxleTrailerIndicatorLit")]
        public bool LiftAxleTrailerIndicatorLit { get; internal protected set; }
        
        [JsonProperty("hShifterSlot")]
        public uint HShifterSlot { get; internal protected set; }
        
        [JsonProperty("damages")]
        public Objects.Truckler.Damages Damages { get; internal protected set; }
        
        [JsonProperty("actualDamages")]
        public Objects.Truckler.Damages ActualDamages { get; internal protected set; }
        
        [JsonProperty("hShifterSelectorFlags")]
        public bool[] HShifterSelectorFlags { get; internal protected set; }
        
        [JsonProperty("wheels")]
        public Objects.Truckler.FrameData.Wheel[] Wheels { get; internal protected set; }
        

        internal protected Truck()
        {
            Damages = new Damages();
            ActualDamages = new Damages();
            Location = new Placement();
            Wheels = new Wheel[0];
            HShifterSelectorFlags = new bool[0];

            Odometer = 0.00f;
            Speed = 0.00f;
            EngineEnabled = false;
            Gear = 0;
            GearDisplayed = 0;
            EngineRPM = 0.00f;
            RetarderBrakeLevel = 0;
            BrakeTemperature = 0.00f;
            FuelRange = 0.00f;
            OilPressure = 0.00f;
            OilTemperature = 0.00f;
            WaterTemperature = 0.00f;
            BatteryVoltage = 0.00f;
            WipersOn = false;
            ParkingBrake = false;
            MotorBrake = false;
            ElectricsEnabled = false;
            InputSteering = 0.00f;
            InputThrottle = 0.00f;
            InputBrake = 0.00f;
            InputClutch = 0.00f;
            EffectiveSteering = 0.00f;
            EffectiveThrottle = 0.00f;
            EffectiveBrake = 0.00f;
            EffectiveClutch = 0.00f;
            CruiseControl = 0.00f;
            AirPressure = 0.00f;
            AirPressureWarning = false;
            AirPressureEmergency = false;
            FuelCurrentLitres = 0.00f;
            FuelWarning = false;
            FuelConsumptionAverage = 0.00f;
            AdBlue = 0.00f;
            AdBlueWarning = false;
            AdBlueConsumptionAverage = 0.00f;
            OilPressureWarning = false;
            WaterTemperatureWarning = false;
            BatteryVoltageWarning = false;
            LightLowBeams = false;
            LightHighBeams = false;
            LightLeftBlinkerLogical = false;
            LightRightBlinkerLogical = false;
            LightHazardWarningLogical = false;
            LightLeftBlinker = false;
            LightRightBlinker = false;
            LightParking = false;
            LightAuxiliaryFront = 0;
            LightAuxiliaryRoof = 0;
            LightBeacon = false;
            LightBrake = false;
            LightReverse = false;
            LightDasboardBacklightFactor = 0.00f;
            DifferentialLockEnabled = false;
            LiftAxleLifted = false;
            LiftAxleIndicatorLit = false;
            LiftAxleTrailerLifted = false;
            LiftAxleTrailerIndicatorLit = false;
            HShifterSlot = 0;
        }
    }
}
