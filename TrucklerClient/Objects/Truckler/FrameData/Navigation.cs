﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Truckler.Objects.Truckler.FrameData
{
    public class Navigation
    {
        [JsonProperty("distance")]
        public float Distance { get; internal protected set; }
        [JsonProperty("time")]
        public float ETA { get; internal protected set; }
        [JsonProperty("speedLimit")]
        public float SpeedLimit { get; internal protected set; }
        internal protected Navigation()
        {
            Distance = 0.00f;
            ETA = 0.00f;
            SpeedLimit = 0.00f;
        }
    }
}
